"""" vim: set ts=3 sts=3 sw=3 tw=200 fdm=marker foldmarker="<,"> filetype=vim :

" Configuration permettant de rédiger en français.
" Les outils de correction orthographique et grammaticale doivent être installé par ailleurs.

" aide 
" <F8>     - active/désactive la correction orthographique
" ]s       - mot suivant
" [s       - mot précédent
" z=       - sur un mot pour afficher la liste des suggestions
" Ctrl-x s - auto complétion à partir du dictionnaire
" zg       - ajoute le mot au dictionnaire
" zug      - retire le mot du dictionnaire
" :help spell

" défini le français comme langue par défaut
set spl=fr


"" Correction orthographique
"  Alterner le mode de correction orthographique
function! ToggleSpell()
	if &spell
		set nospell
	else
		set spell
	end
endfunction

" active/désactive la correction
noremap  <F8> :call ToggleSpell()<cr>

