"""" vim: set ts=3 sts=3 sw=3 tw=200 fdm=marker foldmarker="<,"> filetype=vim :

" Ce qui DOIT être tout à la fin du fichier vimrc

" Génération des fichiers d'aide. DOIT être tout à la fin du fichier .vimrc
" Chargez tous les plugins maintenant. Les plugins doivent être ajoutés au runtimepath
" avant de pouvoir générer des balises d'aide.
packloadall

" Chargez tous les helptags maintenant, après que les plugins aient été chargés.
" Tous les messages et erreurs seront ignorés.
silent! helptags ALL

