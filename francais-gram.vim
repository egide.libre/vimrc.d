"""" vim: set ts=3 sts=3 sw=3 tw=200 fdm=marker foldmarker="<,"> filetype=vim :
"" correction grammaticale
" Grammalecte doit être installé et le plugin vim aussi
" https://github.com/dpelle/vim-Grammalecte

" spécifie le chemin vers grammalecte
let g:grammalecte_cli_py='/usr/bin/grammalecte-cli.py'

noremap  <F7> :GrammalecteCheck<cr>
noremap  <F6> :GrammalecteClear<cr>
