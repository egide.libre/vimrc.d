Changelog
=========
Tous les changements notables apportés à ce projet seront documentés
dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/fr/1.0.0),
et ce projet adhère à la [version sémantique](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security


## [0.1] yyyy-mm-jj
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

