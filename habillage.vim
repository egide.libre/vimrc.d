"""" vim: set ts=3 sts=3 sw=3 tw=200 fdm=marker foldmarker="<,"> filetype=vim :
""" Affichages, thèmes et couleurs

""" ligne de statut (en dessous de la zone de texte)
" Sur deux lignes
set laststatus=2

" personnalisation de la barre de statut
" TODO: expliquer le contenu de la commande... Je ne m'en souviens plus
set statusline=%<%f%h%m%r%=%{&ff}\ %l,%c%V\ %P

" affiche la position courante au sein du fichier
set ruler

" affiche la commande en cours
set showcmd


""" Numérotation des lignes
" Numérote les lignes
set number

" les numéros de lignes sont relatifs à la ligne actuelle
set relativenumber


""" Thème et couleurs
" Fond sombre
set background=dark

" thème de couleurs
colorscheme desert


