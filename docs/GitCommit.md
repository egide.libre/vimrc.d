Règles de commit pour GIT
=========================

Selon un article original de [DotnetDojo](https://www.dotnetdojo.com/git-commit/)


Afin que l'historique de ce projet soit utilisable dans le temps j'estime important que les messages soient formatés suivant certaines règles.

Les buts :
* avoir un contenu homogène dans les commits, qui ne dépende pas de l'humeur, du stress ou de la personne ;
* une capacité d'être analysé par un humain et par un script basique ;
* permettre une entrée (ou une reprise après une longue pause) plus facile dans le projet.


Réaction d'un message de commit
-------------------------------

Chaque message est enregistré selon la forme suivante:

	<type>(<portée>): <sujet>
	<LIGNE BLANCHE>
	<corp>
	<LIGNE BLANCHE>
	<pied>

Où:

- **type** est une description codée de l'action du commit qui DOIT être choisie parmi les valeurs suivantes qui signalent :
	- **feat2s** : l'ajout d'une nouvelle fonctionnalité,
	- **fix** : la correction d'un bogue ou d'une erreur dans le code ou le contenu textuel,
	- **docs** : des modifications ou ajouts uniquement dans la documentations,
	- **style** : les modifications ne portent que sur le style ou le formatage du code et pas sur un changement de sens du code,
	- **refactor** : un changement qui n'est ni une nouvelle fonctionnalité ni une correction de bogue,
	- **perf** : une modification qui améliore les performances ou qui simplifie le code sans changer le sens ou le résulta,
	- **chore** : des modifications dans les outils annexes ou sur les processus de compilations,
	- **test** : des modifications ou des ajouts sur les tests ;
- **portée** indique les composants ou le périmètre des modifications (le nom d'un fichier, d'un composant, une idée comme "pagination"...) ;
- **Sujet** une description succincte sans majuscule et ponctuation finale avec des verbes à l'impératif ou au participe passé ;
- **corp** décrit les raisons qui ont amené à faire ces changement par rapport au commit précédent ;
- **pied** est une option qui permet d'indiquer un numéro de bogue, de décrire le bogue ou de signaler les changements qui casse la rétro compatibilité.

Il serait bon d'établir (ici) une liste des **portées** utilisables.
