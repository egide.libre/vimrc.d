Guide de contribution
=====================

WorkFlow
--------

Ce projet utilise Git-flow au pied de la lettre.

L'article de base qui donnera naissance au projet :

* http://nvie.com/posts/a-successful-git-branching-model/


Aide mémoire français (et en d'autres traductions) :

* https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html


Le module git qui permet de l'appliquer :

* https://github.com/petervanderdoes/gitflow-avh


Nom des versions
----------------

Ce dépôt suis très strictement le modèle de gestion sémantique des version en v2.0

* https://semver.org/lang/fr/spec/v2.0.0.html


Messages de commit
------------------

Selon un article original de [DotnetDojo](https://www.dotnetdojo.com/git-commit/)

Afin que l'historique de ce projet soit utilisable dans le temps j'estime important que les messages soient formatés suivant certaines règles.

Les buts :
* avoir un contenu homogène dans les commits, qui ne dépende pas de l'humeur, du stress ou de la personne ;
* une capacité d'être analysé par un humain et par un script basique ;
* permettre une entrée (ou une reprise après une longue pause) plus facile dans le projet.


### Réaction d'un message de commit

Chaque message est enregistré selon la forme suivante:

	<type>(<portée>): <sujet>
	<LIGNE BLANCHE>
	<corp>
	<LIGNE BLANCHE>
	<pied>

Où:

- **type** est une description codée de l'action du commit qui DOIT être choisie parmi les valeurs suivantes qui signalent :
	- **feat** : l'ajout d'une nouvelle fonctionnalité,
	- **fix** : la correction d'un bogue ou d'une erreur dans le code ou le contenu textuel,
	- **docs** : des modifications ou ajouts uniquement dans la documentations,
	- **style** : les modifications ne portent que sur le style ou le formatage du code et pas sur un changement de sens du code,
	- **refactor** : un changement qui n'est ni une nouvelle fonctionnalité ni une correction de bogue,
	- **perf** : une modification qui améliore les performances ou qui simplifie le code sans changer le sens ou le résulta,
	- **chore** : des modifications dans les outils annexes ou sur les processus de compilations,
	- **test** : des modifications ou des ajouts sur les tests ;
- **portée** indique les composants ou le périmètre des modifications (le nom d'un fichier, d'un composant, une idée comme "pagination"...) ;
- **Sujet** une description succincte sans majuscule et ponctuation finale avec des verbes à l'impératif ou au participe passé ;
- **corp** décrit les raisons qui ont amené à faire ces changement par rapport au commit précédent ;
- **pied** est une option qui permet d'indiquer un numéro de bogue, de décrire le bogue ou de signaler les changements qui casse la rétro compatibilité.

Il serait bon d'établir (ici) une liste des **portées** utilisables.


Contributions
-------------

Libre à vous de cloner le dépôt... Et de proposer des modifications.



