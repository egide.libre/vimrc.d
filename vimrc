"""" vim: set ts=3 sts=3 sw=3 tw=200 fdm=marker foldmarker="<,"> filetype=vim :
" Fichier .vimrc de fra Egidio
" Créé le 11 mai 1993
" Mise à jour : 27/08/2020
" Version 1.9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Dossier de la configuration
let VIMD = "source ~/.vim/vimrc.d/"

set enc=utf-8
set fileencoding=utf-8

"" DOIT être au début du vimrc
" gestionnaire de plugin
execute VIMD . "vundle.vim"

" Affichage et thème
execute VIMD . "habillage.vim"
" Corrections orthographiques
execute VIMD . "francais-orto.vim"
" Corrections grammaticales
"execute VIMD . "francais-gRam.vim"




" Ce qui, pour une raison ou une autre DOIT être tout à la fin du vimrc
execute VIMD . "EndFile.vim"

