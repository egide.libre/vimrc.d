"""" vim: set ts=3 sts=3 sw=3 tw=200 fdm=marker foldmarker="<,"> filetype=vim :
" Cette configuration devrait être au début du fichier vimc, pour je ne sais quelle raison.

" Pour installer Vundle:
"   $ mkdir -p ~/.vim/bundle/ && cd ~/.vim/bundle/ && git clone https://github.com/VundleVim/Vundle.vim

" Aide rapide
"   :PluginList         - lists configured plugins
"   :PluginInstall      - installs plugins; append `!` to update or just :PluginUpdate
"   :PluginSearch foo   - searches for foo; append `!` to refresh local cache
"   :PluginClean        - confirms removal of unused plugins; append `!` to auto-approve removal

set nocompatible                  " be iMproved, required
filetype off                      " required
set rtp+=~/.vim/bundle/Vundle.vim " set the runtime path to include Vundle and initialize
call vundle#begin()               " Les plugin DOIVENT être placés après cette ligne

" Liste des plugins
Plugin 'VundleVim/Vundle.vim'     " let Vundle manage Vundle, required
"Plugin 'dpelle/vim-Grammalecte'   " corrections grammaticales

" Fin de la liste des plugin

call vundle#end()                 " Les plugins doivent être placée avant cette ligne
filetype plugin indent on         " required
"filetype plugin on               " To ignore plugin indent changes, instead use:

