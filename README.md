vimrc
=====

*Date de création* : 2020/08/27

Gestion de mon fichier de configuration Vim pour pouvoir le partager sur mes différentes machines.


Destinataires du projet
-----------------------

Ce dépôt est dédié à un usage strictement personnel. Il n'est cependant pas confidentiel, tout le monde peut servir, mais n'attendez pas de moi que je le maintienne au-delà de mes propres besoins.


Ce que le projet fait
---------------------

Gère un répertoire ~/.Vim/vimrc.d contenant la configuration locale de Vim.

Le vimrc peut vite contenir plusieurs centaines de lignes concerner des sujets très divers. Pour simplifier les choses, j'aimerais répartir les différents sujets dans autant de fichiers qui sont sourcés dans le document maître.


Prérequis
---------

### Logiciel
- GIT
- VIM


Documentation
-------------

1. [LICENSE](LICENSE) termes de la licence
2. [CHANGELOG](CHANGELOG.md) Changements importants d'une version à l'autre
3. [CONTRIBUTING](CONTRIBUTING.md) indications utiles pour les personnes qui voudraient contribuer au projet
4. [INSTALL](INSTALL.md) instructions de configuration, de compilation et d'installation
5. [MANIFEST](docs/MANIFEST) liste des fichiers


### Documentation généraliste
#### Git
Ce projet suit scrupuleusement:
* la gestion sémantique de version.
  * https://semver.org/lang/fr/
* GitFlow
  *  https://danielkummer.github.io/git-flow-cheatsheet/index.fr_FR.html
* les [règles de commit](docs/GitCommit.md)


